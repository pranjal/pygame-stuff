import pygame, sys

pygame.init()
screen = pygame.display.set_mode((640, 480))

white = 210, 210, 210
black = 90, 90, 90

cells = []
for i in range(8):
    cells.append([])
    for j in range(8):
        cells[-1].append(pygame.Rect(80+(j*60), (i*60), 60, 60))

for i in range(len(cells)):
    for j in range(len(cells[i])):
        if (i + j) % 2 == 0:
            screen.fill(white, cells[i][j])
        else:
            screen.fill(black, cells[i][j])
white_names = ["king_w.png", "queen_w.png", "horse_w.png", "bishop_w.png", "rook_w.png", "pawn_w.png"]
black_names = ["king_b.png", "queen_b.png", "horse_b.png", "bishop_b.png", "rook_b.png", "pawn_b.png"]

white_pieces = [pygame.image.load("chess/"+i).convert_alpha() for i in white_names]
black_pieces = [pygame.image.load("chess/"+i).convert_alpha() for i in black_names]
order = (4, 2, 3, 1, 0, 3, 2, 4)
for i in range(8):
    screen.blit(black_pieces[order[i]], (80+(60*i), 0))
for i in range(8):
    screen.blit(black_pieces[-1], (80+(60*i),60))
for i in range(8):
    screen.blit(white_pieces[-1], (80+(60*i),360))
for i in range(8):
    screen.blit(white_pieces[order[i]], (80+(60*i), 420))

selected = None

clock = pygame.time.Clock()
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            x, y = event.pos
            i, j = (x-80)//60, y//60
            if selected:
                screen.fill(black, pygame.Rect(selected, (60, 60)))
                if selected[1] == 0:
                    screen.blit(black_pieces[order[(selected[0]-80)//60]], ((i*60)+80, j*60))
                selected = None
            else:
                selected = (i*60)+80, j*60
    pygame.display.update()
    clock.tick(30)
