import sys, pygame, time, socket, pickle, selectors, threading
LEFT = 0
RIGHT = 1
conn = None
port = 9999
addr = None

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('', port))
s.listen(1)
s.setblocking(0)
def socket_send_ball(socket):
    while socket.fileno() != -1:
        send = pickle.dumps(ball.topleft)
        send_header = bytes((0x01, len(send)))
        socket.sendall(send_header + send)
        time.sleep(0.1)


class Player(pygame.sprite.Sprite):
    def __init__(self, side):
        super().__init__()
        sides = (0, screen_w-player_w)
        self.rect = pygame.Rect(sides[side], (screen_h - player_h)/2, player_w, player_h)
        screen.fill(white, self.rect)

    def update(self, distance):
        dirty_rects.append(self.rect)
        screen.fill(black, self.rect)
        self.rect = self.rect.move(0, distance)
        screen.fill(white, self.rect)
        dirty_rects.append(self.rect)

def lose(close):
    if close:
        conn.sendall(b'\x00')
        conn.close()
    over = pygame.freetype.Font(None, 50).render("GAME OVER", white)
    over[1].x, over[1].y = (screen_w - over[1].w)//2, (screen_h - over[1].h)//2
    screen.blit(*over)
    pygame.display.update()
    while not pygame.event.peek(pygame.QUIT):
        pygame.event.clear()
        clock.tick(60)
    pygame.quit()
    sys.exit()



pygame.init()

screen_w, screen_h = 1280, 720
player_w, player_h = 20, 80
ball_r = 20 #radius
ball_v = [6, 6] #ball velocity
player_v = 8 #player velocity
font_size = 50
pygame.key.set_repeat(1, 20)
black = 0, 0, 0
white = 255, 255, 255

screen = pygame.display.set_mode((screen_w, screen_h))#, pygame.RESIZABLE)

#ball.centerx needs to be >= (ball_r - ball_v) else game over
ball = pygame.draw.circle(screen, white, (ball_r, (screen_h-ball_r)/2), ball_r)


player1 = Player(LEFT)

clock = pygame.time.Clock()
dirty_rects = [player1]

while True:
    try:
        conn, addr = s.accept()
        break
    except: pass
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                player1.update(-player_v)

            elif event.key == pygame.K_s:
                player1.update(player_v)

    screen.fill(black, ball)
    dirty_rects.append(ball)

    if ball.colliderect(player1):
        screen.fill(white, player1)
        ball_v[0] = abs(ball_v[0])
        dirty_rects.append(player1)

    ball = pygame.draw.circle(screen, white, ball.move(ball_v).center, ball_r)
    dirty_rects.append(ball)

    if (ball.centerx - ball_r) < 0:
        lose(False)

    if (ball.centery - ball_r) < 0 or (ball.centery + ball_r) > screen_h:
        ball_v[1] = -ball_v[1]
    if (ball.centerx + ball_r) > screen_w:
        ball_v[0] = -abs(ball_v[0])

    pygame.display.update(dirty_rects)
    dirty_rects.clear()
    clock.tick(60)

send = pickle.dumps([ball, ball_v, player1])
send_header = bytes((len(send),))
conn.sendall(send_header + send)
s.close()
player2 = Player(RIGHT)
dirty_rects.append(player2)
y = threading.Thread(target=socket_send_ball, args=(conn,))
y.start()
selector = selectors.SelectSelector()
selector.register(conn, selectors.EVENT_READ)

while True:
    move = 0
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            conn.sendall(b"\x00")
            conn.close()
            pygame.quit()
            sys.exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                move -= 1
                player1.update(-player_v)

            elif event.key == pygame.K_s:
                move += 1
                player1.update(player_v)
    if move:
        move = str(move)
        send = bytes((0x00, len(move)))
        conn.sendall(send + move.encode('utf-8'))
    

    while selector.select(0):
        header = conn.recv(1)
        data_len = header[0]
        if data_len == 0:
            lose(True)
        data = conn.recv(data_len)
        while len(data) < data_len:
            data += conn.recv(data_len - len(data))
        player2.update(player_v * int(data))


    screen.fill(black, ball)
    dirty_rects.append(ball)

    if ball.colliderect(player1):
        screen.fill(white, player1)
        ball_v[0] = abs(ball_v[0])
        dirty_rects.append(player1)

    elif ball.colliderect(player2):
        screen.fill(white, player2)
        ball_v[0] = -abs(ball_v[0])
        dirty_rects.append(player2)

    ball = pygame.draw.circle(screen, white, ball.move(ball_v).center, ball_r)
    dirty_rects.append(ball)

    if (ball.centerx - ball_r) < 0 or (ball.centerx + ball_r) > screen_w:
        lose(True)

    if (ball.centery - ball_r) < 0 or (ball.centery + ball_r) > screen_h:
        ball_v[1] = -ball_v[1]

    pygame.display.update(dirty_rects)
    dirty_rects.clear()
    clock.tick(60)
