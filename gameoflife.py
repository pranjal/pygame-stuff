import sys, pygame, random, time
from collections import defaultdict

pygame.init()

screen_w, screen_h = 640, 480
black = 0, 0, 0
white = 255, 255, 255
cell_w = cell_h = 10
grid_w, grid_h = screen_w // cell_w, screen_h // cell_h

screen = pygame.display.set_mode((640, 480), pygame.RESIZABLE)
offsets = ((0, -1), (0, 1), (1, 0), (-1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1))

def get_alive_dead_n(alive_cells, x, y):
    neighbours = [(x + dx, y + dy) for dx, dy in offsets]
    dead_n = [cell for cell in neighbours if cell not in alive_cells]
    return len(neighbours) - len(dead_n), dead_n

screen_cells = set()
alive_cells = set()
dead_cells = defaultdict(int)
dirty_rects = []

for x in range(grid_w):
    for y in range(grid_h):
        cell = x, y
        screen_cells.add(cell)
        if random.randint(0, 1):
            alive_cells.add(cell)
            rect = pygame.Rect(x * cell_w, y * cell_h, cell_w, cell_h)
            screen.fill(white, rect)
            dirty_rects.append(rect)

clock = pygame.time.Clock()

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.VIDEORESIZE:
            dx = int((event.w - screen_w)/(cell_w * 2))
            dy = int((event.h - screen_h)/(cell_h * 2))
            alive_cells = {(x+dx, y+dy) for x,y in alive_cells}
            screen_w, screen_h = event.size
            screen.fill(black)
            screen_cells.clear()
            for x in range(screen_w // cell_w):
                for y in range(screen_h // cell_h):
                    screen_cells.add((x,y))
                    if (x, y) in alive_cells:
                        screen.fill(white, pygame.Rect(x*cell_w, y*cell_h, cell_w, cell_h))
            pygame.display.update()

    prev_alive_cells = alive_cells.copy()

    for i in prev_alive_cells:
        alive_n, dead_n = get_alive_dead_n(prev_alive_cells, i[0], i[1])
        for cell in dead_n:
            dead_cells[cell] += 1

        if (alive_n != 2) and (alive_n != 3):
            alive_cells.remove(i)
            if i in screen_cells:
                rect = pygame.Rect(i[0] * cell_w, i[1] * cell_h, cell_w, cell_h)
                screen.fill(black, rect)
                dirty_rects.append(rect)

    for i in dead_cells:
        if dead_cells[i] == 3:
            alive_cells.add(i)
            if i in screen_cells:
                rect = pygame.Rect(i[0] * cell_w, i[1] * cell_h, cell_w, cell_h)
                screen.fill(white, rect)
                dirty_rects.append(rect)

    dead_cells.clear()

    pygame.display.update(dirty_rects)
    dirty_rects.clear()

    clock.tick(10)
