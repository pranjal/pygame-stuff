import sys, pygame, time, socket, pickle, selectors
LEFT = 0
RIGHT = 1

class Player(pygame.sprite.Sprite):
    def __init__(self, side):
        super().__init__()
        sides = 0, screen_w-player_w
        self.rect = pygame.Rect(sides[side], (screen_h - player_h)/2, player_w, player_h)
        screen.fill(white, self.rect)
    def update(self, distance):
        dirty_rects.append(self.rect)
        screen.fill(black, self.rect)
        self.rect = self.rect.move(0, distance)
        screen.fill(white, self.rect)
        dirty_rects.append(self.rect)
def lose(send=False):
    if send:
        conn.sendall(b"\x00")
    conn.close()
    over = pygame.freetype.SysFont(None, 50).render("GAME OVER", white)
    over[1].x, over[1].y = (screen_w - over[1].w)/2, (screen_h - over[1].h)/2
    screen.blit(*over)
    pygame.display.update(over[1])
    while not pygame.event.peek(pygame.QUIT):
        pygame.event.clear()
        clock.tick(60)
    pygame.quit()
    sys.exit()
port = 9999

addr = (input('Enter IP: '), port)
conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.connect(addr)

pygame.init()

screen_w, screen_h = 1280, 720
player_w, player_h = 20, 80
player_v = 8 #player velocity
pygame.key.set_repeat(1, 20)
ball_r = 20
ball_size = (ball_r*2, ball_r*2)
black = 0, 0, 0
white = 255, 255, 255

screen = pygame.display.set_mode((screen_w, screen_h))

player2 = Player(RIGHT)

clock = pygame.time.Clock()

data_len = conn.recv(1)[0]
data = conn.recv(data_len)
while len(data) < data_len:
    data += conn.recv(data_len - len(data))
ball, ball_v, player1 = pickle.loads(data)
dirty_rects = [player1, player2]
screen.fill(white, player1)

selector = selectors.SelectSelector()
selector.register(conn, selectors.EVENT_READ)

while True:
    move = 0
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            conn.sendall(b'\x00')
            conn.close()
            pygame.quit()
            sys.exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                move -= 1
                player2.update(-player_v)

            elif event.key == pygame.K_DOWN:
                move += 1
                player2.update(player_v)

    if move:
        move = str(move)
        send = bytes((len(move),))
        conn.sendall(send + move.encode('utf-8'))

    screen.fill(black, ball)
    dirty_rects.append(ball)

    if ball.colliderect(player1):
        screen.fill(white, player1)
        ball_v[0] = abs(ball_v[0])
        dirty_rects.append(player1)

    elif ball.colliderect(player2):
        screen.fill(white, player2)
        ball_v[0] = -abs(ball_v[0])
        dirty_rects.append(player2)

    while selector.select(0):
        header = conn.recv(2)
        msg_type = header[0]
        if len(header) == 1 and msg_type == 0:
            lose()
        data_len = header[1]
        data = conn.recv(data_len)
        while len(data) < data_len:
            data += conn.recv(data_len - len(data))
        if msg_type == 0:
            player1.update(player_v*int(data))
        elif msg_type == 1:
            ball = pygame.Rect(pickle.loads(data), ball_size)
        else:
            assert(False)

    ball = pygame.draw.circle(screen, white, ball.move(ball_v).center, ball_r)
    dirty_rects.append(ball)

    if (ball.centery - ball_r) < 0 or (ball.centery + ball_r) > screen_h:
        ball_v[1] = -ball_v[1]

    pygame.display.update(dirty_rects)
    dirty_rects.clear()
    clock.tick(60)

