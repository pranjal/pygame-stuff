import pygame, time, sys, math

pygame.init()

screen = pygame.display.set_mode((1280, 720))
clock = pygame.time.Clock()
white = 255, 255, 255
black = 0, 0, 0

t = time.localtime()
hour = t.tm_hour % 12
min = t.tm_min
sec = t.tm_sec

angle = hour * 30
pygame.draw.circle(screen, white, (640, 360), 200)
dirty_rect = pygame.draw.circle(screen, black, (640, 360), 195)
hour_hand = min_hand = sec_hand = pygame.Rect(0,0,0,0)

hour_dict = {0: (0, -190), 1: (95, -165), 2: (165, -95), 3: (190, 0), 4: (165, 95), 5: (95, 165), 6: (0, 190), 7: (-95, 165), 8: (-165, 95), 9: (-190, 0), 10: (-165, -95), 11: (-95, -165)}

min_sec_dict = dict()
for i in range(60):
    min_sec_dict[i] = 640+round(190*math.sin(i*math.pi/30)), 360-round(190*math.cos(i*math.pi/30))

for key in hour_dict:
    hour_dict[key] = hour_dict[key][0] + 640, hour_dict[key][1] + 360

pygame.display.update()

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
    t = time.localtime()
    hour = t.tm_hour % 12
    min = t.tm_min
    sec = t.tm_sec
    screen.fill(black, hour_hand)
    screen.fill(black, min_hand)
    screen.fill(black, sec_hand)
    hour_hand = pygame.draw.aaline(screen, white, (640, 360), hour_dict[hour])
    min_hand = pygame.draw.aaline(screen, white, (640, 360), min_sec_dict[min])
    sec_hand = pygame.draw.aaline(screen, white, (640, 360), min_sec_dict[sec])
    pygame.display.update(dirty_rect)
    clock.tick(1)
