import pygame, sys, random

pygame.init()
screen_dim = 1280, 720
screen = pygame.display.set_mode(screen_dim, pygame.RESIZABLE)
UP, LEFT, DOWN, RIGHT = range(4)
direction_dict = {pygame.K_w: UP, pygame.K_a: LEFT, pygame.K_s: DOWN, pygame.K_d: RIGHT}
cell_size = (5, 5)
snake_size = 5
snake = [pygame.Rect(640+(i*cell_size[0]), 360, cell_size[0], cell_size[1]) for i in range(snake_size)]
white = 255, 255, 255
black = 0, 0 , 0
direction = RIGHT

for i in snake:
    screen.fill(white, i)
clock = pygame.time.Clock()
move_dict = {UP: (0, -5), LEFT: (-5, 0), DOWN: (0, 5), RIGHT: (5, 0)}
food = pygame.Rect((200, 200), cell_size)
screen.fill(white, food)
dirty_rects = [*snake, food]

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        elif (event.type == pygame.KEYDOWN) and (event.key in direction_dict) and (abs(direction_dict[event.key] - direction) != 2):
            direction = direction_dict[event.key]
        elif event.type == pygame.VIDEORESIZE:
            screen.fill(black)
            screen.fill(white, food)
            for rect in snake:
                screen.fill(white, rect)
            dirty_rects.append(screen.get_rect())

    snake.append(snake[-1].move(move_dict[direction]))

    if snake[-1] != food:
        screen.fill(white, snake[-1])
        dirty_rects.append(snake[-1])
        screen.fill(black, snake[0])
        dirty_rects.append(snake.pop(0))
    else:
        food = pygame.Rect(random.randint(0,100)*cell_size[0], random.randint(0,100)*cell_size[1], cell_size[0], cell_size[1])
        screen.fill(white, food)
        dirty_rects.append(food)

    pygame.display.update(dirty_rects)
    dirty_rects.clear()

    clock.tick(40)
