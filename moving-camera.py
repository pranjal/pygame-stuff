import pygame, sys, random

pygame.init()
screen = pygame.display.set_mode((640, 480), pygame.RESIZABLE)
#dirty_rects = []
white = 255, 255, 255
black = 0, 0, 0
player = pygame.Rect(640, 480, 20, 20)
font_size = 48
pygame.key.set_repeat(1, 20)
onscreen = []
obstacle = pygame.Rect(random.randint(100, 200)+1366, 480, 20, 20)
jump = False

clock = pygame.time.Clock()
dirty_rects = [player]
screen.fill(white, player)
font = pygame.font.Font(pygame.font.get_default_font(), font_size)
over = font.render("GAME OVER", True, white)
lose = False
score = 0
score_sprite = font.render(f"Score: {score}", True, white)
score_xy = 0, 0
speed = 2
while True:
    score += 1
    speed = min(speed + 0.01, 10)
    screen.fill(black, score_sprite.get_rect())
    score_sprite = font.render(f"Score: {score}", True, white)
    screen.blit(score_sprite, score_xy)
    dirty_rects.append(score_sprite.get_rect())
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                jump = True
                #screen.fill(whi)
            #if event.key == pygame.K_d:
            #    screen.fill(black, player)
            #    player = player.move(5, 0)
            #    onscreen[0] = player
            #    screen.fill(white, player)
        elif event.type == pygame.VIDEORESIZE:
            screen.fill(white, player)
            dirty_rects.append(player)
    if jump:
        if player.y >= 400:
            screen.fill(black, player)
            dirty_rects.append(player)
            player = player.move(0, -5)
            screen.fill(white, player)
            dirty_rects.append(player)
        else:
            jump = False
    elif player.y < 480:
        screen.fill(black, player)
        dirty_rects.append(player)
        player = player.move(0, 5)
        screen.fill(white, player)
        dirty_rects.append(player)
    obstacle = obstacle.move(-speed, 0)
    if obstacle.x <= screen.get_width():
        onscreen.append(obstacle)
        obstacle = pygame.Rect((random.randint(3, 5)*100)+screen.get_width(), 480, 20, 20)
    shift = 0
    for i in range(len(onscreen)):
        i -= shift
        if player.colliderect(onscreen[i]):
            over_xy = (screen.get_width() - over.get_width())/2, (screen.get_height() - over.get_height())/2
            screen.blit(over, over_xy)
            dirty_rects.append(pygame.Rect(over_xy, over.get_size()))
            pygame.display.update(dirty_rects)
            while not pygame.event.peek(pygame.QUIT):
                pygame.event.clear()
                clock.tick(60)
            pygame.quit()
            sys.exit()
        dirty_rects.append(onscreen[i])
        screen.fill(black, onscreen[i])
        onscreen[i] = onscreen[i].move(-speed, 0)
        if onscreen[i].x <= 0:
            del onscreen[i]
            shift += 1
        else:
            screen.fill(white, onscreen[i])
            dirty_rects.append(onscreen[i])

    pygame.display.update(dirty_rects)
    dirty_rects.clear()
    clock.tick(60)
