import sys, pygame, socket, selectors

class Player(pygame.sprite.Sprite):
    def __init__(self, *dim):
        super().__init__()
        self.rect = pygame.Rect(*dim)
        screen.fill(white, self.rect)

    def update(self, distance, header=None):
        if header is not None:
            conn.sendall(header)
        dirty_rects.append(self.rect)
        screen.fill(black, self.rect)
        self.rect = self.rect.move(distance)
        screen.fill(white, self.rect)
        dirty_rects.append(self.rect)

def lose():
    over = pygame.freetype.Font(None, 50).render("GAME OVER", white)
    over[1].x, over[1].y = (screen_w - over[1].w)/2, (screen_h - over[1].h)/2
    screen.blit(*over)
    pygame.display.update()
    restart = False
    while not restart:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                conn.sendall(b"\x09")
                conn.close()
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_r:
                restart = True
        clock.tick(60)

    while selector.select(0):
        header = conn.recv(1)[0]
        if header == 9:
            pygame.quit()
            sys.exit()
        elif (header >> 7) != 1:
            players[(header-1)//2].update(move_dict[header])

    conn.sendall(b"\x11")
    while not selector.select(0):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                conn.sendall(b"\x09")
                conn.close()
                pygame.quit()
                sys.exit()
        clock.tick(60)
    byte = conn.recv(1)
    if byte == b"\x09":
        conn.close()
        pygame.quit()
        sys.exit()
    assert(byte == b"\x11")
    screen.fill(black, over[1])
    dirty_rects.append(over[1])


pygame.init()

conn = socket.create_connection(("127.0.0.1", 9999))
id = conn.recv(1)[0] + 1
screen_w, screen_h = 1280, 640
player_w, player_h = 20, 80
player_v = 80 #player velocity
ball_r = 20
black = 0, 0, 0
white = 255, 255, 255

screen = pygame.display.set_mode((screen_w, screen_h))
ball = pygame.Rect(0, 0, 0, 0)
players = [Player(0, 320, 20, 80), Player(1260, 320, 20, 80), Player(600, 0, 80, 20), Player(600, 620, 80, 20)]
clock = pygame.time.Clock()
dirty_rects = [*players]

selector = selectors.SelectSelector()
selector.register(conn, selectors.EVENT_READ)
if id == 1:
    move_keys = [pygame.K_w, pygame.K_s]
    move = (0, -player_v), (0, player_v)
else:
    move_keys = [pygame.K_a, pygame.K_d]
    move = (-player_v, 0), (player_v, 0)
move_header = bytes(((id*2)+1,)), bytes(((id*2)+2,))
move_dict = {i: (0 if i in (1, 2, 3, 4) else pow(-1, i) * player_v, 0 if i not in (1, 2, 3, 4) else pow(-1, i) * player_v) for i in range(1, 9)}

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            conn.sendall(b"\x09")
            conn.close()
            pygame.quit()
            sys.exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == move_keys[0]:
                players[id].update(move[0], move_header[0])

            elif event.key == move_keys[1]:
                players[id].update(move[1], move_header[1])

    while selector.select(0):
        header = conn.recv(1)[0]
        if (header >> 7) == 1:
            screen.fill(black, ball)
            dirty_rects.append(ball)
            for player in players:
                if ball.colliderect(player):
                    screen.fill(white, player)
                    dirty_rects.append(player)
            center = b""
            while len(center) < 2:
                center += conn.recv(2 - len(center))
            ball = pygame.draw.circle(screen, white, (((header & 127) << 4) + (center[0] >> 4), ((center[0] & 15) << 6) + center[1]), ball_r)
            dirty_rects.append(ball)
        elif header == 9:
            pygame.quit()
            sys.exit()
        elif header == 16:
            lose()
        else:
            players[(header-1)//2].update(move_dict[header])

    pygame.display.update(dirty_rects)
    dirty_rects.clear()
    clock.tick(60)
