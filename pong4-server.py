import sys, pygame, socket, selectors
id = -1

class Player(pygame.sprite.Sprite):
    def __init__(self, *dim):
        super().__init__()

        global id
        self.id = id
        id += 1

        self.rect = pygame.Rect(dim)
        screen.fill(white, self.rect)

    def update(self, distance, header):
        dirty_rects.append(self.rect)
        screen.fill(black, self.rect)
        self.rect = self.rect.move(distance)
        screen.fill(white, self.rect)
        dirty_rects.append(self.rect)
        for i in range(3):
            if i != self.id:
                conns[i].sendall(bytes((header,)))

def lose():
    over = pygame.freetype.Font(None, 50).render("GAME OVER", white)
    over[1].x, over[1].y = (screen_w - over[1].w)//2, (screen_h - over[1].h)//2
    screen.blit(*over)
    pygame.display.update()
    restart = False
    while not restart:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                for conn in conns:
                    conn.sendall(b"\x09")
                    conn.close()
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_r:
                restart = True
        clock.tick(60)

    for conn in conns:
        while conn not in [i[0].fileobj for i in selector.select(0)]:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    for conn in conns:
                        conn.sendall(b"\x09")
                        conn.close()
                    pygame.quit()
                    sys.exit()
            clock.tick(60)
        byte = conn.recv(1)
        if byte == b"\x09":
            conn.close()
            for _conn in conns:
                if _conn != conn:
                    _conn.sendall(b"\x09")
                    _conn.close()
            pygame.quit()
            sys.exit()
        assert(byte == b"\x11")

    broadcast(b"\x11")
    global ball
    global ball_v
    screen.fill(black, over[1])
    screen.fill(black, ball)
    dirty_rects.append(over[1])
    dirty_rects.append(ball)
    ball_v = [4, 4]
    ball = pygame.draw.circle(screen, white, (30, 30), ball_r)
    dirty_rects.append(ball)

def broadcast(msg):
    for conn in conns:
        conn.sendall(msg)

selector = selectors.SelectSelector()
conns = []

s = socket.create_server(('', 9999))
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

for i in range(3):
    conns.append(s.accept()[0])
    selector.register(conns[-1], selectors.EVENT_READ)

s.close()
pygame.init()

screen_w, screen_h = 1280, 640
ball_r = 20 #radius
ball_v = [4, 4] #ball velocity
player_v = 80 #player velocity
black = 0, 0, 0
white = 255, 255, 255

screen = pygame.display.set_mode((screen_w, screen_h))

#ball.centerx needs to be >= (ball_r - ball_v) else game over
ball = pygame.draw.circle(screen, white, (30, 30), ball_r)

players = Player(0, 320, 20, 80), Player(1260, 320, 20, 80), Player(600, 0, 80, 20), Player(600, 620, 80, 20)

clock = pygame.time.Clock()
dirty_rects = [*players]
move_dict = {i: (0 if i in (1, 2, 3, 4) else pow(-1, i) * player_v, 0 if i not in (1, 2, 3, 4) else pow(-1, i) * player_v) for i in range(3, 9)}


for i in range(3):
    conns[i].sendall(bytes((i,)))

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            for conn in conns:
                conn.sendall(b"\x10")
                conn.close()
            pygame.quit()
            sys.exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                players[0].update((0, -player_v), 1)

            elif event.key == pygame.K_s:
                players[0].update((0, player_v), 2)

    broadcast(bytes((128 | (ball.centerx >> 4), ((ball.centerx & 15) << 4) + (ball.centery >> 6), ball.centery & 63)))

    while True:
        readable = [i[0].fileobj for i in selector.select(0)]
        if not readable:
            break
        for conn in readable:
            header = conn.recv(1)[0]
            if header == 9:
                for _conn in conns:
                    if _conn != conn:
                        _conn.sendall(b"\x09")
                lose()
            else:
                players[(header-1)//2].update(move_dict[header], header)

    screen.fill(black, ball)
    dirty_rects.append(ball)

    for i in range(4):
        if ball.colliderect(players[i]):
            screen.fill(white, players[i])
            ball_v[i//2] = -ball_v[i//2]
            dirty_rects.append(players[i])

    ball = pygame.draw.circle(screen, white, ball.move(ball_v).center, ball_r)
    dirty_rects.append(ball)

    if (ball.centerx - ball_r) < 0 or (ball.centerx + ball_r) > screen_w or (ball.centery - ball_r) < 0 or (ball.centery + ball_r) > screen_h:
        broadcast(b"\x10")
        lose()

    pygame.display.update(dirty_rects)
    dirty_rects.clear()
    clock.tick(60)
