import sys, pygame, time

pygame.init()

screen_w, screen_h = 1280, 720
player_w, player_h = 20, 80
ball_r = 20 #radius
ball_v = [6, 6] #ball velocity
player_v = 8 #player velocity
font_size = 50
black = 0, 0, 0
white = 255, 255, 255

screen = pygame.display.set_mode((screen_w, screen_h))

#ball.centerx needs to be >= (ball_r - ball_v) else game over
ball = pygame.draw.circle(screen, white, (ball_r, (screen_h-ball_r)/2), ball_r)

player1_rect = pygame.Rect(0, (screen_h - player_h)/2, player_w, player_h)
player2_rect = player1_rect.move(screen_w - player_w, 0)

player1 = pygame.draw.rect(screen, white, player1_rect)
player2 = pygame.draw.rect(screen, white, player2_rect)

clock = pygame.time.Clock()
dirty_rects = [player1, player2]
font = pygame.freetype.SysFont(None, font_size)

#sway defaults are delay 600, interval 25
pygame.key.set_repeat(1, 20)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                dirty_rects.append(player1)
                screen.fill(black, player1)
                player1 = player1.move(0, -player_v)
                screen.fill(white, player1)
                dirty_rects.append(player1)

            elif event.key == pygame.K_s:
                dirty_rects.append(player1)
                screen.fill(black, player1)
                player1 = player1.move(0, player_v)
                screen.fill(white, player1)
                dirty_rects.append(player1)

            elif event.key == pygame.K_UP:
                dirty_rects.append(player2)
                screen.fill(black, player2)
                player2 = player2.move(0, -player_v)
                screen.fill(white, player2)
                dirty_rects.append(player2)

            elif event.key == pygame.K_DOWN:
                dirty_rects.append(player2)
                screen.fill(black, player2)
                player2 = player2.move(0, player_v)
                screen.fill(white, player2)
                dirty_rects.append(player2)

    screen.fill(black, ball)
    dirty_rects.append(ball)

    if ball.colliderect(player1):
        screen.fill(white, player1)
        ball_v[0] = abs(ball_v[0])
        dirty_rects.append(player1)

    elif ball.colliderect(player2):
        screen.fill(white, player2)
        ball_v[0] = -abs(ball_v[0])
        dirty_rects.append(player2)

    ball = pygame.draw.circle(screen, white, ball.move(ball_v).center, ball_r)
    dirty_rects.append(ball)

    if (ball.centerx - ball_r) < 0 or (ball.centerx + ball_r) > screen_w:
        over = font.render("GAME OVER", white)
        over[1].x = (screen_w - over[1].w)//2
        over[1].y = (screen_h - over[1].h)//2
        screen.blit(over[0], over[1])
        pygame.display.update(over[1])
        while not pygame.event.peek(pygame.QUIT):
            pygame.event.clear()
            clock.tick(60)
        sys.exit()

    if (ball.centery - ball_r) < 0 or (ball.centery + ball_r) > screen_h:
        ball_v[1] = -ball_v[1]

    pygame.display.update(dirty_rects)
    dirty_rects.clear()
    clock.tick(60)
