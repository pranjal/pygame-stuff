import pygame, sys
pygame.init()
screen = pygame.display.set_mode((1280, 720), pygame.RESIZABLE)
white = 255, 255, 255
black = 0, 0 , 0

clock = pygame.time.Clock()
font_size = 24
font = pygame.font.Font(pygame.font.get_default_font(), font_size)
#	[[, [[, acpid, add-shell, addgroup, adduser, adjtimex, arch, arp, arping, ash, awk, base64, basename, bbconfig, bc, beep,
#	blkdiscard, blkid, blockdev, brctl, bunzip2, bzcat, bzip2, cal, cat, chattr, chgrp, chmod, chown, chpasswd, chroot, chvt,
#	cksum, clear, cmp, comm, cp, cpio, crond, crontab, cryptpw, cut, date, dc, dd, deallocvt, delgroup, deluser, depmod, df, diff,
#	dirname, dmesg, dnsdomainname, dos2unix, du, dumpkmap, echo, ed, egrep, eject, env, ether-wake, expand, expr, factor,
#	fallocate, false, fatattr, fbset, fbsplash, fdflush, fdisk, fgrep, find, findfs, flock, fold, free, fsck, fstrim, fsync, fuser,
#	getopt, getty, grep, groups, gunzip, gzip, halt, hd, head, hexdump, hostid, hostname, hwclock, id, ifconfig, ifdown, ifenslave,
#	ifup, init, inotifyd, insmod, install, ionice, iostat, ip, ipaddr, ipcalc, ipcrm, ipcs, iplink, ipneigh, iproute, iprule,
#	iptunnel, kbd_mode, kill, killall, killall5, klogd, last, less, link, linux32, linux64, ln, loadfont, loadkmap, logger, login,
#	logread, losetup, ls, lsattr, lsmod, lsof, lsusb, lzcat, lzma, lzop, lzopcat, makemime, md5sum, mdev, mesg, microcom, mkdir,
#	mkdosfs, mkfifo, mkfs.vfat, mknod, mkpasswd, mkswap, mktemp, modinfo, modprobe, more, mount, mountpoint, mpstat, mv, nameif,
#	nanddump, nandwrite, nbd-client, nc, netstat, nice, nl, nmeter, nohup, nologin, nproc, nsenter, nslookup, ntpd, od, openvt,
#	partprobe, passwd, paste, pgrep, pidof, ping, ping6, pipe_progress, pivot_root, pkill, pmap, poweroff, printenv, printf, ps,
#	pscan, pstree, pwd, pwdx, raidautorun, rdate, rdev, readahead, readlink, realpath, reboot, reformime, remove-shell, renice,
#	reset, resize, rev, rfkill, rm, rmdir, rmmod, route, run-parts, sed, sendmail, seq, setconsole, setfont, setkeycodes,
#	setlogcons, setpriv, setserial, setsid, sh, sha1sum, sha256sum, sha3sum, sha512sum, showkey, shred, shuf, slattach, sleep,
#	sort, split, stat, strings, stty, su, sum, swapoff, swapon, switch_root, sync, sysctl, syslogd, tac, tail, tar, tee, test,
#	time, timeout, top, touch, tr, traceroute, traceroute6, true, truncate, tty, ttysize, tunctl, udhcpc, udhcpc6, umount, uname,
#	unexpand, uniq, unix2dos, unlink, unlzma, unlzop, unshare, unxz, unzip, uptime, usleep, uudecode, uuencode, vconfig, vi, vlock,
words = ["volname", "watch", "watchdog", "wc", "wget", "which", "who", "whoami", "whois", "xargs", "xxd", "xzcat", "yes", "zcat"]
rendered = [font.render(word, True, white) for word in words]
word_rects = [render.get_rect() for render in rendered]
for i in range(len(word_rects)):
    word_rects[i] = word_rects[i].move(0, i*100)

matching = words.copy()
typed = ""


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key in range(pygame.K_a, pygame.K_z+1):
                typed += chr(event.key)
                for i in range(len(words)):
                    if typed == words[i]:
                        typed = ""
                        del words[i]
                        del rendered[i]
                        screen.fill(black, word_rects[i])
                        del word_rects[i]
                        break
            elif event.key == pygame.K_BACKSPACE:
                typed = typed[:-1]
    for i in range(len(words)):
        screen.fill(black, word_rects[i])
        word_rects[i] = word_rects[i].move(1, 0)
        screen.blit(rendered[i], word_rects[i])
    pygame.display.update()
    clock.tick(60)
