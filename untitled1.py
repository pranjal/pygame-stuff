import sys, pygame

pygame.init()
width, height = 1280, 720
speed = [5, 5]
black = 0, 0, 0
white = 255, 255, 255
DEAD = 0
ALIVE = 1

screen = pygame.display.set_mode((width,height))

class enemy:
    def __init__(self):
        self.state = ALIVE
        self.rect = pygame.Rect(0, 0, 20, 20)

player = pygame.draw.rect(screen, white, (0, 0, 40, 40))
foods = []
dirty_rects = [player]
for i in range(20):
    foods.append(enemy())
    screen.fill(white, foods[i].rect)
    dirty_rects.append(foods[i].rect)
fps = pygame.time.Clock()
score = 0
cur = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                dirty_rects.append(player)
                screen.fill(black, player)
                player = player.move([0, -40])
                screen.fill(white, player)
                dirty_rects.append(player)
            elif event.key == pygame.K_s:
                dirty_rects.append(player)
                screen.fill(black, player)
                player = player.move([0, 40])
                screen.fill(white, player)
                dirty_rects.append(player)
            elif event.key == pygame.K_a:
                dirty_rects.append(player)
                screen.fill(black, player)
                player = player.move([-40, 0])
                screen.fill(white, player)
                dirty_rects.append(player)
            elif event.key == pygame.K_d:
                dirty_rects.append(player)
                screen.fill(black, player)
                player = player.move([40, 0])
                screen.fill(white, player)
                dirty_rects.append(player)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            if not player.colliderect(foods[cur].rect):
                dirty_rects.append(foods[cur].rect.copy())
                screen.fill(black, foods[cur].rect)
            foods[cur].rect.x, foods[cur].rect.y = event.pos
            screen.fill(white, foods[cur].rect)
            dirty_rects.append(foods[cur].rect)
            foods[cur].state = ALIVE
            cur = (cur+1)%len(foods)
    fps.tick(40)
    
    for i in range(len(foods)):
        if foods[i].state == DEAD:
            continue
        elif player.colliderect(foods[i].rect):
            dirty_rects.append(foods[i].rect)
            screen.fill(black, foods[i].rect)
            dirty_rects.append(player)
            screen.fill(white, player)
            foods[i].state = DEAD
            score += 1
        else:
            dirty_rects.append(foods[i].rect)
            screen.fill(white, foods[i].rect)

    pygame.display.update(dirty_rects)
    dirty_rects.clear()
