import pygame, random

pygame.init()

width, height = 1280, 720
size = (width, height)

screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()

black = 0, 0, 0
white = 255, 255, 255

player = pygame.draw.rect(screen, white, (0, (height-60)/2, 20, 60))
dirty_rects = [player]
time_passed = 0


speed = [-5, 0]
enemies = []

for i in range(1, 10):
    enemies.append(pygame.Rect(width, 72*i, 20, 20))

shots = []

enemies_count = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP and player.top > 0:
                screen.fill(black, player)
                dirty_rects.append(player)
                player = player.move([0, -20])
                screen.fill(white, player)
                dirty_rects.append(player)
            elif event.key == pygame.K_DOWN and player.bottom < height:
                screen.fill(black, player)
                dirty_rects.append(player)
                player = player.move([0, 20])
                screen.fill(white, player)
                dirty_rects.append(player)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            shots.append(pygame.Rect(player.x, player.y, 20, 20))
            screen.fill(white, shots[-1])
            dirty_rects.append(shots[-1])

    #time_passed += clock.get_time()
    #if time_passed >= 5:
    #    time_passed = 0
    #    pygame.draw.rect(screen, white, enemies[enemies_count])
    #    enemies_count = (enemies_count + 1) % 9
    
    minus = 0
    for i in range(len(shots)):
        if i > len(shots) - minus:
            break
        screen.fill(black, shots[i])
        dirty_rects.append(shots[i])
        shots[i] = shots[i].move([5,0])
        if shots[i].x > width:
            del shots[i]
            minus += 1
            continue
        screen.fill(white, shots[i])
        dirty_rects.append(shots[i])
    
    for i in range(9):
        screen.fill(black, enemies[i])
        dirty_rects.append(enemies[i])
        for j in range(len(shots)):
            if shots[j].colliderect(enemies[i]):
                break
        else:
            enemies[i] = enemies[i].move(speed)
            screen.fill(white, enemies[i])
            dirty_rects.append(enemies[i])
        if enemies[i].left < 0:
            screen.fill(black, enemies[i].copy())
            dirty_rects.append(enemies[i].copy())
            enemies[i].update(width, random.randint(0, (height-20)//20)*20, 20, 20)

    pygame.display.update(dirty_rects)
    clock.tick(30)
    dirty_rects.clear()
