import pygame

def rotate(matrix):
    return [list(i)[::-1] for i in zip(*matrix)]

pygame.init()
screen_dim = 1280, 720
screen = pygame.display.set_mode(screen_dim, pygame.RESIZABLE)
white = 255, 255, 255
black = 0, 0, 0

clock = pygame.time.Clock()
font_size = 24
font = pygame.font.SysFont("DejaVu Sans Mono", font_size)

matrix = [[1,2,3],[4,5,6],[7,8,9]]

rendered = []
rects = []
for i in matrix:
    rendered.append([])
    rects.append([])
    for j in i:
        rendered[-1].append(font.render(str(j), True, white))
        rects[-1].append(rendered[-1][-1].get_rect())

for i in range(len(rects)):
    for j in range(len(rects[i])):
        rects[i][j] = rects[i][j].move(640+(j*50),360+(i*50))

orig_rects = rects[0][0].copy(), rects[0][1].copy()
dirty_rect = pygame.Rect(640, 360, 100+rects[0][2].width, 100+rects[2][0].height)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

    screen.fill(black, dirty_rect)

    for k in range(0, len(rects), 2):
        for m in range(0, len(rects[k]), 2):
            if k+m == 0: move = (1, 0) #pos x
            elif k+m == 4: move = (-1, 0) #neg x
            elif k == 2: move = (0, -1) #neg y
            else: move = (0, 1) #pos y
            rects[k][m] = rects[k][m].move(move)

    if rects[1][0] != orig_rects[1]:
        for i in range(0, 3, 2):
            move_first = (1,1) if i == 0 else (-1,-1)
            move_second = (1,-1) if i == 0 else (-1, 1)
            rects[i][1] = rects[i][1].move(move_first)
            rects[1][i] = rects[1][i].move(move_second)

    for i in range(len(rects)):
        for j in range(len(rects[i])):
            screen.blit(rendered[i][j], rects[i][j])

    if rects[2][0] == orig_rects[0]:
        rects = rotate(rects)
        rendered = rotate(rendered)
        orig_rects = rects[0][0], rects[0][1]

    pygame.display.update(dirty_rect)
    clock.tick(30)
